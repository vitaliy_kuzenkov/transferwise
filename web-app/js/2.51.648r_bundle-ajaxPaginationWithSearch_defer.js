var AjaxPaginationWithSearch = function(options) {
	this.options = {
		entitiesList: '',
		showOlderLink: '',
		total: '',
		offset: '',
		loadEntitiesUrl: '',
		userId: '',
		searchField: '',
		debounceDelay: 300
	};
	jQuery.extend(this.options, options);
	this.init();
}

AjaxPaginationWithSearch.prototype = {
	init: function() {
		var self = this;
		this.switchShowOlderAvailability();
		this.previousSearch = ''

		this.options.showOlderLink.click(function() {
			self.loadEntities()
			return false;
		})

		this.searchField = this.options.searchField
		this.shouldBeCleaned = false
		var formUtils = new FormUtils()
		var ajaxUtils = new AjaxUtils()
		formUtils.listenForChange(this.searchField)

		this.searchField.change(ajaxUtils.debounce(function() {
			if (self.previousSearch == self.searchField.val()) return
			self.options.offset = 0
			self.shouldBeCleaned = true
			self.loadEntities()
		}, this.options.debounceDelay))
	},

	switchShowOlderAvailability: function() {
		if (this.options.offset >= this.options.total) {
			this.options.showOlderLink.hide()
		} else {
			this.options.showOlderLink.show()
		}
	},

	loadEntities: function() {
		var self = this;
		var params = {}
		params.offset = this.options.offset
		params.id = this.options.userId
		params.q = self.searchField.val()
		this.previousSearch = params.q
		jQuery.get(this.options.loadEntitiesUrl, params, function(data) {
			if (data.offset != params.offset) return;
			self.options.offset += data.count;
			self.options.total = data.total;
			if (self.shouldBeCleaned) {
				self.shouldBeCleaned = false;
				self.options.entitiesList.find('.data-row').remove();
			}

			if (self.options.entitiesList.find('.data-row').size() > 0) {
				self.options.entitiesList.find('.data-row:last').after(data.rows);
			} else {
				self.options.entitiesList.append(data.rows)
			}

			self.switchShowOlderAvailability();
		}, 'json')
	}
}

