var CalculatorHelpChatOpener = function(options) {
	this.options = {
		selectors: [],
		usdCurrencyId: 3
	};
	jQuery.extend(this.options, options);
	this.init();
};

CalculatorHelpChatOpener.prototype = {
	init: function() {
		if (typeof SnapABug === 'undefined') return;
		if (typeof SnapABug === 'undefined') return;

		this.bindClickEventsToSelectors();
	},

	bindClickEventsToSelectors: function() {
		var self = this;

		jQuery.each(self.options.selectors, function(index, value) {
			value.change(function() {
				self.openChatWindowIfRequired(jQuery(this).val());
			})
		});
	},

	openChatWindowIfRequired: function(currentSelectedCurId) {
		var self = this;

		if (typeof SnapABug === 'undefined') return;
		if (typeof currentSelectedCurId === 'undefined') return;
		if (currentSelectedCurId != self.options.usdCurrencyId) return;

		setTimeout("SnapABug.openProactiveChat(false, false, 'Hi and thanks for visiting! If there is anything you need help with, we are here to assist you!');", 5 * 1000);
	}
};


