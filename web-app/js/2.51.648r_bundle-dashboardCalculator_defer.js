var ExchangeCalculator = function(block, options) {
	this.form = jQuery(block);
	this.options = {
		debounceDelay: 300,
		isAdmin: false,
		inactiveElementClass: 'disabled',
		currenciesData: [],
//		show "other" option in currency selectors
		showOtherOptionInCurSelectors: false,
		widget: false,
//		used for GA tracking on partners websites
		locationUrl: ''
	};
	jQuery.extend(this.options, options);
	this.init();
};

ExchangeCalculator.prototype = {
	init: function() {
		var self = this;

		self.showScrollOSX();

		this.form.submit(function() {
			return self.checkLimits();
		});
		var ajaxUtils = new AjaxUtils();

		this.sourceInput = this.form.find("input[name=sourceValue]");
		this.targetInput = this.form.find("input[name=targetValue]");
		this.fixedAmountCheckbox = this.form.find("input[name=isFixTypeTarget]");
		this.fixTypeField = this.form.find("input[name=fixType]");
		this.linkToComparePage = this.form.find('.linkToComparePage');

		var calculateWithDebounce = ajaxUtils.debounce(function() {
			self.calculate();
		}, this.options.debounceDelay);

		this.sourceInput.keyup(calculateWithDebounce);
		this.fixedAmountCheckbox.click(function() {
			if (self.options.widget) {
				LogUtils.sendGoogleAnalyticsEventWithOption('calc-widget', 'click_fix_type_checkbox', self.options.locationUrl);
			} else {
				LogUtils.sendGoogleAnalyticsEvent('make_transfer', 'click_fix_type_checkbox');
			}
			self.setActiveAmountBox();
		});
		this.targetInput.keyup(calculateWithDebounce);
		this.sourceInput.focus(function() {
			self.fixedAmountCheckbox.attr('checked', false);
			self.setActiveAmountBox();
		});
		this.targetInput.focus(function() {
			if (self.isFixSourcePaymentOnly()) {
				self.sourceInput.focus();
				return false;
			}

			if (self.options.widget) {
				LogUtils.sendGoogleAnalyticsEventWithOption('calc-widget', 'click_target_value_field', self.options.locationUrl);
			} else {
				LogUtils.sendGoogleAnalyticsEvent('make_transfer', 'click_target_value_field');
			}
			self.fixedAmountCheckbox.attr('checked', true);
			self.setActiveAmountBox();
		});
		this.sourceCurrencySelector = this.form.find('select.sourceCurrency');
		this.sourceCurrencySelector.selectmenu();
		this.sourceCurrencySelector.change(function() {
			if(self.otherOptionIsChosen()) {
				self.openCurrencyWishesDialog();
				return;
			}
			self.handleCurrencyChanging(true);
		});

		this.targetCurrencySelector = this.form.find('select.targetCurrency');
		this.targetCurrencySelector.selectmenu();
		this.targetCurrencySelector.change(function() {
			if(self.otherOptionIsChosen()) {
				self.openCurrencyWishesDialog();
				return;
			}
			self.handleCurrencyChanging(false);
		});

		this.invertDirectionControl =  this.form.find('a.invert-direction');
		this.invertDirectionControl.click(function() {
			self.invertDirection();
			return false;
		});

		this.offeredRate = this.form.find('.results');
		this.fullComparison = this.form.find('.full_comparison');
		if (this.options.externalSubmitButton) {
			this.options.externalSubmitButton.click(function(event) {
				self.form.submit();
			})
		}
		this.updateSourceCurrencyList();
		this.updateTargetCurrencyList();
		this.setActiveAmountBox();
	},

	setActiveAmountBox: function() {
		if (this.fixedAmountCheckbox.is(":checked")) {
			this.fixTypeField.val(this.options.fixTypeTarget);
		} else {
			this.fixTypeField.val(this.options.fixTypeSource);
		}

		this.sourceInput.removeClass(this.options.inactiveElementClass);
		this.targetInput.removeClass(this.options.inactiveElementClass);
		if (this.fixTypeField.val() == this.options.fixTypeTarget) {
			this.sourceInput.addClass(this.options.inactiveElementClass);
		} else {
			this.targetInput.addClass(this.options.inactiveElementClass);
		}
		this.calculate();
	},

	invertDirection: function() {
		var sourceValue = this.sourceCurrencySelector.val();
		this.sourceCurrencySelector.val(this.targetCurrencySelector.val());
		this.targetCurrencySelector.val(sourceValue);
		this.sourceCurrencySelector.selectmenu("value", this.sourceCurrencySelector.val());
		this.targetCurrencySelector.selectmenu("value", this.targetCurrencySelector.val());
		this.sourceCurrencySelector.change();
	},

	calculate: function() {
		var self = this;
		if (this.previousFormValues == this.form.serialize()) return;

		var spinerZero = '0.00';
		self.form.find('.calc_number').html(spinerZero);
		this.updateCompareLink();
		jQuery.post(this.options.calculateActionUrl, this.form.serialize(), function(json) {
			if(self.fixTypeField.val() == self.options.fixTypeTarget) {
				self.sourceInput.val(json.sourceValue);
			} else {
				self.targetInput.val(json.targetValue);
			}
			self.offeredRate.html(json.priceReview);
			self.fullComparison.html(json.fullComparisonLink);

			self.previousFormValues = self.form.serialize();
			self.initialiseListeners();
			self.updateTextsOnPage(json);
		}, 'json')
	},

	isExpanded: function () {
		return this.form.find('.service-cost-details').is(':visible');
	},

	toggleBlocks: function () {
		var self = this;
		this.form.find('.service-cost-details').slideToggle('fast', function() {
			var links = self.form.find('.col-1 .show-service-cost-details');
			if (self.isExpanded()) {
				links.addClass('opened');
			} else {
				links.removeClass('opened');
			}
		});
	},

	hideServiceCostDetails: function() {
		this.form.find('.service-cost-details').hide();
		this.form.find('.col-1 .show-service-cost-details').removeClass('opened');
	},

	initialiseListeners: function() {
		var self = this;
		this.offeredRate.find('.rateCheck a').click(function() {
			LogUtils.sendGoogleAnalyticsEvent('check_rate', self.sourceCurrencySelector.val())
		});
		this.form.find('.show-service-cost-details').click(function() {
			self.toggleBlocks();
			if (!$('.show-service-cost-details.opened').is(':visible')) {
				LogUtils.sendGoogleAnalyticsEvent('calculator', 'expand_the_fees_row');
			}
			return false;
		});
	},

	updateTextsOnPage: function(json) {
		var empty = '0.00';
		if (jQuery.format.number(json.sourceValue.toString()) <= jQuery.format.number(json.bankFee.toString())) {
			jQuery('.bank_you_get').html(empty);
		}
		jQuery('.' + this.options.sourceZoneClass).html(json.sourceCurrencyZone);
		jQuery('.' + this.options.sourceCurrencyCode).html(json.sourceCurrencyCode);
		jQuery('.' + this.options.targetCurrencyCode).html(json.targetCurrencyCode);
		jQuery('.' + this.options.sourceCurrencySymbol).html(json.sourceCurrencySymbol);
		jQuery('.' + this.options.targetCurrencySymbol).html(json.targetCurrencySymbol);
		jQuery('.' + this.options.sourceValue).html(json.sourceValue);
		jQuery('.' + this.options.targetValue).html(json.targetValue);
		jQuery('.' + this.options.savingValue).html(json.savingValue);
		jQuery('.' + this.options.savingCurrencySymbol).html(json.savingSymbol);
		jQuery('.' + this.options.twFee).val(json.fee).trigger('change');
		jQuery('.' + this.options.bankFee).val(json.bankFee).trigger('change');
		jQuery('.' + this.options.savingInputValue).val(json.savingValue).trigger('change');
		if (jQuery.format.number(json.sourceValue.toString()) <= 0 ) {
			jQuery('.calc_number').html(empty);
		}
	},

	isFixSourcePaymentOnly: function() {
		var pair = this.options.currenciesData['ccy'][this.sourceCurrencySelector.val()].tgt[this.targetCurrencySelector.val()];
		return pair.so >= 0;
	},

	handleFixTypeSwitcherAvailability: function() {
		if (!this.fixedAmountCheckbox.get(0)) return;
		if (this.isFixSourcePaymentOnly()) {
			this.fixTypeField.val(this.options.fixTypeSource);
			this.fixedAmountCheckbox.attr('checked', false).attr('disabled', 'disabled');
			this.setActiveAmountBox();
		} else {
			this.fixedAmountCheckbox.attr('disabled', false);
		}
	},

	updateSourceCurrencyList: function() {
		var newValues = {};
		$.each(this.options.currenciesData['ccy'], function(key, value) {
			newValues[key] = value.cd.toUpperCase();
		});
		this.updateCurrencyList(this.sourceCurrencySelector, newValues)
	},

	updateTargetCurrencyList: function() {
		var newValues = {};
		$.each(this.options.currenciesData['ccy'][this.sourceCurrencySelector.val()].tgt, function(key, value) {
			newValues[key] = value.cd.toUpperCase();
		});
		newValues[this.sourceCurrencySelector.val()] = this.sourceCurrencySelector.find('option:checked').text();
		this.updateCurrencyList(this.targetCurrencySelector, ArrayUtils.sortArrayByKeys(newValues));
	},

	updateCurrencyList: function(select, newValues) {
		var selectedValue = select.val();
		select.empty();
		$.each(newValues, function(key, value) {
			if (value) select.append('<option value="' + key + '">' + value + '</option>');
		});
		if (this.options.showOtherOptionInCurSelectors) {
			select.append('<option value="Other" class="other-cur-option">Other</option>');
		}
		select.val(selectedValue);
		if (this.options.widget || DeviceUtils.isDesktop()) {
			select.selectmenu('destroy');
			select.selectmenu();
		}
	},

	handleCurrencyChanging: function(sourceChanged) {
		var formUtils = new FormUtils();
		if (sourceChanged) {
			if (this.sourceCurrencySelector.val() == this.targetCurrencySelector.val()) {
				formUtils.selectNextItemFromStyledDropdown(this.targetCurrencySelector, false);
			}
			this.updateTargetCurrencyList();
		} else if (this.sourceCurrencySelector.val() == this.targetCurrencySelector.val()) {
			formUtils.selectNextItemFromStyledDropdown(this.sourceCurrencySelector, true);
			return;
		}

		this.handleFixTypeSwitcherAvailability();

		this.calculate();

		if (this.sourceCurrencySelector.val() == 1) {
			$('.make-transferwise a.iframe.youtube').attr('href', 'https://www.youtube.com/embed/1PM8It-5bVQ?rel=0&vq=hd720');
		} else if (this.sourceCurrencySelector.val() == 2) {
			$('.make-transferwise a.iframe.youtube').attr('href', 'https://www.youtube.com/embed/ztN7gSqr2HM?rel=0&vq=hd720');
		}
	},

	updateCompareLink: function() {
		var link = this.options.compareLinks[this.targetCurrencySelector.find('option:selected').text()];

		if (link) {
			link = link + '?amount=' + this.sourceInput.val() + "&sourceCurrency=" + this.sourceCurrencySelector.find('option:selected').text()
		} else {
			link = this.options.compareLinks['default'];
			if (this.options.detailedCompareLinks) {
				link += '?amount=' + this.sourceInput.val();
				link +=  "&sourceCurrency=" + this.sourceCurrencySelector.find('option:selected').text();
				link +=  "&targetCurrency=" + this.targetCurrencySelector.find('option:selected').text();
				link +=  "&iframe=true"
			}
		}

		this.linkToComparePage.attr('href', link)
	},

	checkLimits: function() {
		var targetCurrency = this.sourceInput.val();
		var value = jQuery.format.number(this.sourceInput.val());
		var targetValue = jQuery.format.number(this.targetInput.val());

		if (isNaN(value)) {
			alert(this.options.notNumberMessage);
			return false;
		}

		if (targetValue <= 0) {
			alert(this.options.targetUnderLimitMessage);
			return false;
		}

		if (this.options.isAdmin) return true;

		var pair = this.options.currenciesData['ccy'][this.sourceCurrencySelector.val()];
		if (value > pair.lm && pair.lm) {
			this.trackOverLimitEvent(value, pair.cd);
			alert(Mustache.render(this.options.currenciesData.msg.uplim, {limit: pair.lm, currencyCode: pair.cd.toUpperCase()}));
			return false;
		}
		var lowLimitObj = pair.tgt[this.targetCurrencySelector.val()];
		if (value < lowLimitObj.lowl && lowLimitObj.lowl) {
			alert(Mustache.render(this.options.currenciesData.msg.lowlim, {limit: lowLimitObj.lowl, currencyCode: pair.cd.toUpperCase()}));
			return false;
		}

		return true;
	},

	trackOverLimitEvent: function(value, currency) {
		if(!window._gaq) return;
		if (self.options.widget) {
			LogUtils.trackOverLimitEvent('calc-widget', currency, value);
		} else {
			LogUtils.trackOverLimitEvent('Index page', currency, value);
		}
	},

	otherOptionIsChosen: function() {
		if(this.targetCurrencySelector.val() == "Other") return true;
		if(this.sourceCurrencySelector.val() == "Other") return true;

		return false
	},

	openCurrencyWishesDialog: function() {
		$('a#fire-cur-wish-pop').click();
		LogUtils.sendGoogleAnalyticsEvent('CurrencyWishesPopUp', 'seen');
		//reset calculator to default currencies
		this.sourceCurrencySelector.val('2');
		this.targetCurrencySelector.val('1');
		this.updateSourceCurrencyList();
		this.updateTargetCurrencyList();
	},

	showScrollOSX: function() {
		if (navigator.appVersion.indexOf("Mac")!=-1) {
			$('[name=targetCurrencyId]').selectmenu("widget").addClass('showScrollOSX');
			$('[name=sourceCurrencyId]').selectmenu("widget").addClass('showScrollOSX');
		}
	}
};


