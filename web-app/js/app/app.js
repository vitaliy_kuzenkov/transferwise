var app = angular.module('accountApp', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'js/app/views/account.html',
            controller: 'accountController'
        })
});
