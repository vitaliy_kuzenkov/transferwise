app.service('calculatorService', function ($http) {
        this.sourceCurrencies = function () {
            return [
                {id: 1, name: 'EUR'},
                {id: 2, name: 'GBP'},
                {id: 4, name: 'PLN'},
                {id: 5, name: 'CHF'},
                {id: 6, name: 'NOK'},
                {id: 7, name: 'SEK'},
                {id: 8, name: 'DKK'},
                {id: 15, name: 'HUF'},
                {id: 31, name: 'RON'},
                {id: 40, name: 'CZK'},
            ]
        }

        this.targetCurrencies = function () {
            return [
                {id: 1, name: 'EUR'},
                {id: 2, name: 'GBP'},
                {id: 3, name: 'USD'},
                {id: 4, name: 'PLN'},
                {id: 5, name: 'CHF'},
                {id: 6, name: 'NOK'},
                {id: 7, name: 'SEK'},
                {id: 8, name: 'DKK'},
                {id: 9, name: 'AUD'},
                {id: 14, name: 'GEL'},
                {id: 15, name: 'HUF'},
                {id: 24, name: 'HKD'},
                {id: 26, name: 'INR'},
                {id: 30, name: 'RON'},
                {id: 31, name: 'TRY'},
                {id: 32, name: 'NZD'},
                {id: 35, name: 'SGD'},
                {id: 36, name: 'ZAR'},
                {id: 40, name: 'CZK'},
                {id: 41, name: 'BGN'},
            ];
        }

        this.calculate = function (data) {
            var result = $http.post('/calculator', data).then(function (response) {
                    return response.data;
                },
                function () {
                    if (confirm("Oops, that's new. Please try again by reloading this page. In case if issue remains - let us know and we gladly help you. Would you like to reload this page?")) {
                        window.location.reload(true);
                    }
                });

            return result
        }
    }
)
