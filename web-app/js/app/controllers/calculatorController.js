app.controller('calculatorController', function ($scope, calculatorService) {

    function init() {
        $scope.sourceCurrencies = calculatorService.sourceCurrencies()
        $scope.targetCurrencies = calculatorService.targetCurrencies()
        $scope.sourceCurrency = $scope.sourceCurrencies[0].id
        $scope.targetCurrency = $scope.targetCurrencies[0].id
        $scope.states = ['source', 'target']
        $scope.currentState = $scope.states[0]
        $scope.targetState = $scope.states[1]
        $scope.calculationResults = {
            sourceValue: '1000',
            targetValue: '1000'
        }

        // add functions to scope
        $scope.updateTarget = updateTarget;
        $scope.updateSource = updateSource;
        $scope.setState = setState;
        $scope.calculate = calculate;

        // update values
        updateTarget();

        //update ui elements
        updateUi();
    }

    function updateUi() {
        setTimeout(function () {
            jQuery('#calculateForm select').selectmenu()
        }, 0) // wait for source loading
    }

    function updateTarget() {
        var target = jQuery.grep(calculatorService.targetCurrencies(), function (c) {
            return c.id != $scope.sourceCurrency;
        })[0].id;
        if ($scope.sourceCurrency == $scope.targetCurrency) {
            $scope.targetCurrency = target;
        }
        calculate();
        updateUi();
    }

    function updateSource() {
        var source = jQuery.grep(calculatorService.sourceCurrencies(), function (c) {
            return c.id != $scope.targetCurrency;
        })[0].id;
        if ($scope.targetCurrency == $scope.sourceCurrency) {
            $scope.sourceCurrency = source;
        }
        calculate();
        updateUi();
    }

    function calculate() {
        var data = $scope.calculationResults
        data.sourceCurrency = $scope.sourceCurrency
        data.targetCurrency = $scope.targetCurrency
        data.currentState = $scope.currentState
        data.targetState = $scope.targetState
        calculatorService.calculate(data).then(function (results) {
            $scope.calculationResults = results;
        });
    }

    function setState(state) {
        $scope.currentState = state
        $scope.targetState = jQuery($scope.states).filter(function () {
            return this != state
        })[0]
    }

    // init controller
    init();

});