//used to preset default options/behaviour of JQuery post,get,ajax methods
$.ajaxSetup({
	error: function(xhr, status, err) {
		switch(xhr.status) {
			case 401: {
				onHttpErrorStatus("Unexpected problem during remote call. Probably you are not logged in. Would you like to reload this page?");
				break;
			}
			case 403: {
				onHttpErrorStatus("You don't have enough rights to access this resource, please contact us if you believe that it's misbehave. Would you like to reload this page to try again?");
				break;
			}
			case 404: {
				onHttpErrorStatus("Oops, that's new. Resource you are accessing doesn't exist. Please try again by reloading this page. In case if issue remains - let us know and we gladly help you. Would you like to reload this page?");
				break;
			}
			case 500: {
				onHttpErrorStatus("Oops, that's new. Please try again by reloading this page. In case if issue remains - let us know and we gladly help you. Would you like to reload this page?");
				break;
			}


		}
	}
});

function onHttpErrorStatus(message)
{
	if (confirm(message)) {
		window.location.reload(true);
	}
}

