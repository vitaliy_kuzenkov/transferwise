var ConfirmMessageManager = function (state, disableMessage, enableMessage) {
	this.state = state;
	this.disableMessage = disableMessage
	this.enableMessage = enableMessage
};

ConfirmMessageManager.prototype = {
	manageConfirmMessage: function() {
		var self = this;
		if (self.state == "true") {
			return confirm(self.disableMessage);
		} else {
			return confirm(self.enableMessage);
		}
	}
};


