//Used for selecting content of input field
function initialiseSelectable() {
	jQuery('.selectable').each(function(index, el) {
		if ($(el).data('selectable-initialised')) return;

		$(el).focus(function(e) {
			e.target.select();
		});
		$(el).mouseup(function(e) {
			e.preventDefault();
		});

		$(el).data('selectable-initialised', 1);
	});
}

initialiseSelectable();

//Used for selecting content inside of block
function initialiseSelectableBlock() {
	jQuery('.selectableBlock').each(function(index, el) {
		$(el).click(function(e) {
			if (document.selection) {
				var range = document.body.createTextRange();
				range.moveToElementText(e.target);
				range.select();
			} else if (window.getSelection) {
				var range = document.createRange();
				range.selectNode(e.target);
				window.getSelection().addRange(range);
			}
		});
	});
}

initialiseSelectableBlock();



