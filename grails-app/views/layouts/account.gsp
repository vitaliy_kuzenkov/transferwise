<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<html lang="en">
<head>

    <title>TransferWise - Send Money Abroad - Details</title>

    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="noodp"/>
    <meta name="version" content="2.51.648r"/>
    <meta name="keywords" content=""/>

    <meta name="apple-itunes-app" content="app-id=612261027">


    <meta name="description"
          content="Send money to your friends and family or pay for goods and services in foreign currency. No margins and no spreads on true market exchange rates."/>


    <script type="text/javascript">
        var mixPanelToken = '8ba4a7a5182f05e0a79ded57d5d2f051';
        var baseUri = '/';
    </script>

    <script type="text/javascript">
        function initMixPanel() {

            mixpanel.identify('74590');
            mixpanel.register_once({isRegistered: 'true'});


            if (!window.mixPanelCookie) {
                mixpanel.track('first visit', {url: window.location.pathname});
            }
        }
    </script>

    <!-- Microsoft smarties -->
    <meta http-equiv="ImageResize" content="no"/>
    <meta http-equiv="ImageToolbar" content="no"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Default stylesheets -->
    <link rel="shortcut icon" href="/static/images/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/static/images/TW_logo.png"/>



    <meta property="og:image" content="http://transferwise.com/images/fb-og-logo.png"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <g:layoutHead/>
    <g:javascript library="application"/>
    <r:require module="account"/>
    <r:layoutResources/>

</head>

<body ng-app='accountApp'>
<g:layoutBody/>
<r:layoutResources/>
</body>
</html>
