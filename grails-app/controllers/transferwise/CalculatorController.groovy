package transferwise

import grails.converters.JSON

class CalculatorController {

  def calculate() {
    //face calculation
    //should be in Service but placed here in demo purposes
    def data = request.JSON
    def result = [:]
    String source = data.currentState
    String target = data.targetState
    Float sourceValue = (data[source + 'Value'] ?: 0).toFloat()
    Float exchangeRates = data[source + 'Currency'] / data[target + 'Currency'] * Math.random()
    result[source + 'Value'] = sourceValue
    result[target + 'Value'] = (sourceValue * exchangeRates).round(2)
    result['bankFee'] = ((data.sourceValue ?: 0).toFloat() * Math.random() / data.sourceCurrency).round(2)
    result['fee'] = (result['bankFee'] / 10).round(2)

    render result as JSON
  }
}
