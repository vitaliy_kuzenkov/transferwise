modules = {
  account {
    //CSS
    resource url: 'css/2.51.648r_jquery.ui.core.css'
    resource url: 'css/2.51.648r_jquery.ui.accordion.css'
    resource url: 'css/2.51.648r_jquery.ui.autocomplete.css'
    resource url: 'css/2.51.648r_jquery.ui.button.css'
    resource url: 'css/2.51.648r_jquery.ui.datepicker.css'
    resource url: 'css/2.51.648r_jquery.ui.dialog.css'
    resource url: 'css/2.51.648r_jquery.ui.progressbar.css'
    resource url: 'css/2.51.648r_jquery.ui.resizable.css'
    resource url: 'css/2.51.648r_jquery.ui.selectable.css'
    resource url: 'css/2.51.648r_jquery.ui.slider.css'
    resource url: 'css/2.51.648r_jquery.ui.tabs.css'
    resource url: 'css/2.51.648r_bundle-jui_head.css'
    resource url: 'css/2.51.648r_bundle-forms_head.css'
    resource url: 'css/2.51.648r_bundle-dashboard_common_head.css'
    resource url: 'css/2.51.648r_bundle-base-css_head.css'
    resource url: 'css/2.51.648r_bundle-core_head.css'
    resource url: 'css/2.51.648r_bundle-dashboardCalculator_head.css'
    resource url: 'css/2.51.648r_bundle-tooltipster_head.css'
    resource url: 'css/2.51.648r_bundle-responsive_notifications_head.css'
    resource url: 'css/2.51.648r_bundle-responsive_dashboard_common_m_head.css', attrs:[media:'screen and (min-width:50px) and (max-width:767px)']
    resource url: 'css/2.51.648r_bundle-responsive_dashboard_common_t_head.css', attrs:[media:'screen and (min-width:768px) and (max-width:960px)']
    resource url: 'css/2.51.648r_bundle-fancybox_head.css'
    resource url: 'css/2.51.648r_bundle-setPassword_head.css'
    resource url: 'css/2.51.648r_bundle-responsive_dashboard_m_head.css', attrs:[media:'screen and (min-width:50px) and (max-width:767px)']
    resource url: 'css/2.51.648r_bundle-responsive_dashboard_t_head.css', attrs:[media:'screen and (min-width:768px) and (max-width:960px)']
    resource url: 'css/2.51.648r_bundle-dashboard_head.css'
    resource url: 'css/2.51.648r_tooltipStyles.css'

    //Javascript
    resource url: 'js/2.51.648r_jquery-1.7.1.js'
    //Angular
    resource url: 'js/angular.min.js'
    resource url: 'js/angular-route.min.js'
    resource url: 'js/app/app.js'
    resource url: 'js/app/controllers/accountController.js'
    resource url: 'js/app/controllers/calculatorController.js'
    resource url: 'js/app/services/calculatorService.js'

    resource url: 'js/2.51.648r_bundle-bundle_baseJs_head.js'
    resource url: 'js/2.51.648r_bundle-jui_defer.js'
    resource url: 'js/2.51.648r_bundle-ajaxPaginationWithSearch_defer.js'
    resource url: 'js/2.51.648r_bundle-messages_defer.js'
    resource url: 'js/2.51.648r_bundle-cookie_defer.js'
    resource url: 'js/2.51.648r_bundle-calculatorHelpOpener_defer.js'
    resource url: 'js/2.51.648r_bundle-dashboardCalculator_defer.js'
    resource url: 'js/2.51.648r_bundle-tooltipster_defer.js'
    resource url: 'js/2.51.648r_bundle-fancybox_defer.js'
    resource url: 'js/2.51.648r_bundle-selectable_defer.js'
    resource url: 'js/2.51.648r_bundle-validation_defer.js'
    resource url: 'js/2.51.648r_bundle-defaultValue_defer.js'
    resource url: 'js/2.51.648r_bundle-api_defer.js'
    resource url: 'js/2.51.648r_tooltip-min.js'


  }
}