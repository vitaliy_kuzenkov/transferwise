class UrlMappings {

  static mappings = {
    "/account" {
      controller = "account"
      action = "index"
    }
    "/calculator" {
      controller = "calculator"
      action = "calculate"
    }

    "500"(view: '/error')
    "/"(redirect: "/account")
  }
}
